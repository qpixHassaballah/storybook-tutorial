import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


class App extends Component {
  constructor(props){
    super(props);
    
}
  render() {
    let {title} = this.props;
    let {bg} = this.props;
    if(title === undefined ) {
      console.log('undefined props');
      title="React";
      
    }
    if(bg === undefined){
      console.log('undefined props');
      // title="React";
      bg= "App-header"
    } 
    else {
      console.log('defined props');
      console.log(bg);
    }
    return (
      <div className="App">
        <header className={bg} >
          <img src={logo} className="App-logo" alt="logo" />
          <h1> Welcome to  {title} </h1>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
